<?php

	namespace Sixnapps\EasyadminExtensionBundle\Services;

	use Doctrine\ORM\EntityManagerInterface;

	/**
	 * Class AppServices
	 *
	 * @package Sixnapps\EasyadminExtensionBundle\Services
	 */
	class AppServices
	{

		/**
		 * @var EntityManagerInterface
		 */
		private $em;


		/**
		 * AppServices constructor.
		 *
		 * @param EntityManagerInterface $em
		 */
		public function __construct(EntityManagerInterface $em)
		{
			$this->em = $em;
		}


		/**
		 * @param int $longueur
		 *
		 * @return string
		 */
		public function genererMDP($longueur = 8)
		{
			$mdp         = "";
			$possible    = "34679ACDEFGHJKLMNPQRTVWXY";
			$longueurMax = strlen( $possible );
			if ( $longueur > $longueurMax ) {
				$longueur = $longueurMax;
			}
			$i = 0;
			while ( $i < $longueur ) {
				$caractere = substr( $possible, mt_rand( 0, $longueurMax - 1 ), 1 );

				if ( !strstr( $mdp, $caractere ) ) {
					$mdp .= $caractere;
					$i++;
				}
			}
			return $mdp;
		}


		/**
		 * @param $data
		 *
		 * @return mixed
		 */
		public function toURL($data)
		{
			$data = str_replace( [ '#', '?', '%', ':', '«', '»', '!', '>', '<', '=', '"', '.', ',', '(', ')' ], '', $data );
			$data = str_replace( [ '&eacute;', 'é', 'è', 'ê', 'ë', 'É', 'Ê', 'Ë' ], "e", $data );
			$data = str_replace( [ 'î', 'ï', 'Î', 'Ï' ], 'i', $data );
			$data = str_replace( [ 'û', 'ù', 'ü', 'Û', 'Ù', 'Ü' ], 'u', $data );
			$data = str_replace( [ 'à', 'â', 'ä', 'À', 'Â', 'Ä' ], 'a', $data );
			$data = str_replace( [ 'ô', 'ö', 'Ô', 'Ö' ], 'o', $data );
			$data = str_replace( 'ç', 'c', $data );
			$data = str_replace( [ '&amp;', '&' ], '-et-', $data );
			$data = str_replace( [ "'", "/" ], "-", $data );
			$data = strtolower( $data );
			$data = trim( $data );
			$data = str_replace( '-', ' ', $data );
			$data = preg_replace( '/\s{2,}/', ' ', $data );
			$data = str_replace( " ", "-", $data );
			$data = strip_tags( $data );

			return $data;
		}

	}
