<?php

	namespace Sixnapps\EasyadminExtensionBundle\Services;

	use App\Entity\User;
	use Doctrine\ORM\EntityManagerInterface;
	use FOS\UserBundle\Model\UserManagerInterface;
	use Symfony\Component\HttpFoundation\File\UploadedFile;
	use Symfony\Component\HttpKernel\KernelInterface;

	/**
	 * Class FixturesServices
	 *
	 * @package App\Services
	 */
	class FixturesServices
	{
		/**
		 * @var KernelInterface
		 */
		protected $kernel;
		/**
		 * @var EntityManagerInterface
		 */
		protected $em;
		/**
		 * @var UserManagerInterface
		 */
		protected $um;
		/**
		 * @var null
		 */
		protected $alias;


		/**
		 * FixturesServices constructor.
		 *
		 * @param KernelInterface        $kernel
		 * @param EntityManagerInterface $em
		 * @param UserManagerInterface   $um
		 */
		public function __construct( KernelInterface $kernel, EntityManagerInterface $em, UserManagerInterface $um )
		{
			$this->alias  = NULL;
			$this->kernel = $kernel;
			$this->em     = $em;
			$this->um     = $um;
		}


		/**
		 * @param $className
		 *
		 * @return string
		 * @throws \ReflectionException
		 */
		private function getShortName( $className )
		{
			$class   = new $className();
			$reflect = new \ReflectionClass( $class );
			return lcfirst( $reflect->getShortName() );
		}


		/**
		 * @param object $scope
		 * @param null   $alias
		 *
		 * @throws \ReflectionException
		 */
		public function setting( object $scope, $alias = NULL )
		{
			$this->alias = $alias;
			$className   = $scope->entity;
			$object      = new $className();

			if ( is_subclass_of( $object, 'FOS\UserBundle\Model\User' ) ) {
				$this->setUsers( $scope );
			} else {
				$this->setObjects( $scope );
			}
		}


		/**
		 * @param object $scope
		 *
		 * @throws \ReflectionException
		 */
		public function setObjects( object $scope )
		{
			$className = $scope->entity;
			foreach ( $scope->getDatas() as $nbr => $data ) {
				$object = new $className();
				foreach ( $data as $key => $value ) {
					$setter = 'set' . ucfirst( $key );

					if ( $key == "es_trad" ) {
						foreach ( $value as $key_2 => $value_2 ) {

							$classTranslation = $className . 'Translation';
							$translation      = new $classTranslation();
							$translation
								->setTranslatable( $object )
								->setLocale( $key_2 )
							;
							foreach ( $value_2 as $key_3 => $value_3 ) {

								$setter = 'set' . ucfirst( $key_3 );
								$translation->$setter( $value_3 );
							}
							$this->em->persist( $translation );
						}
					} else {
						if ( is_array( $value ) ) {
							foreach ( $value as $key_2 => $value_2 ) {
								$setter = 'add' . ucfirst( $key );
								$object->$setter( $value_2 );
							}
						} else {
							$object->$setter( $value );
						}
					}
				}
				$this->em->persist( $object );
				if ( is_null( $this->alias ) ) {
					$scope->addReference( $this->getShortName( $className ) . '-' . ( $nbr + 1 ), $object );
				} else {
					$scope->addReference( $this->alias . '-' . ( $nbr + 1 ), $object );
				}
			}
			$this->em->flush();
		}


		/**
		 * @param $scope
		 *
		 * @throws \ReflectionException
		 */
		public function setUsers( $scope )
		{
			$className = $scope->entity;

			foreach ( $scope->getDatas() as $nbr => $data ) {
				/** @var User $object */
				$object = new $className();
				$object->setUsername( md5( uniqid() ) );
				foreach ( $data as $key => $value ) {
					if ( is_array( $value ) ) {
						foreach ( $value as $key_2 => $value_2 ) {
							$setter = 'add' . ucfirst( $key );
							if ( method_exists( $object, $setter ) ) {
								$object->$setter( $value_2 );
							}
						}
					} else {
						$setter = 'set' . ucfirst( $key );
						if ( method_exists( $object, $setter ) ) {
							$object->$setter( $value );
						}
					}
				}
				$scope->addReference( $this->getShortName( $className ) . '-' . ( $nbr + 1 ), $object );

				$this->um->updateUser( $object );

			}
		}


		/**
		 * @param        $basename
		 * @param string $suffixe
		 *
		 * @return UploadedFile
		 */
		public function generateUploadedFile( $basename, $suffixe = '-cp' )
		{
			$path    = $this->kernel->getProjectDir() . '/src/DataFixtures/Files/';
			$ext     = pathinfo( $basename, PATHINFO_EXTENSION );
			$nom     = pathinfo( $basename, PATHINFO_FILENAME );
			$suffixe = $suffixe . '-' . uniqid();

			copy( $path . $basename, $path . $nom . $suffixe . '.' . $ext );

			return new UploadedFile( $path . $nom . $suffixe . '.' . $ext, $nom . $suffixe . '.' . $ext, NULL, NULL, NULL, TRUE );
		}
	}
