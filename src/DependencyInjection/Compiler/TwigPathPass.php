<?php

	namespace Sixnapps\EasyadminExtensionBundle\DependencyInjection\Compiler;

	use EasyCorp\Bundle\EasyAdminBundle\EasyAdminBundle;
	use Sixnapps\EasyadminExtensionBundle\SixnappsEasyadminExtensionBundle;
	use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
	use Symfony\Component\DependencyInjection\ContainerBuilder;

	class TwigPathPass implements CompilerPassInterface
	{
		/**
		 * @param ContainerBuilder $container
		 *
		 * @throws \ReflectionException
		 */
		public function process(ContainerBuilder $container)
		{
			$twigLoaderFilesystemId         = $container->getAlias( 'twig.loader' )->__toString();
			$twigLoaderFilesystemDefinition = $container->getDefinition( $twigLoaderFilesystemId );

			// Replaces native EasyAdmin templates
			$sixnappsEasyadminExtensionBundleRefl = new \ReflectionClass( SixnappsEasyadminExtensionBundle::class );
			if ( $sixnappsEasyadminExtensionBundleRefl->isUserDefined() ) {
				$easyAdminExtensionBundlePath = \dirname( (string) $sixnappsEasyadminExtensionBundleRefl->getFileName() );
				$easyAdminExtensionTwigPath   = $easyAdminExtensionBundlePath . '/Resources/views';
				$twigLoaderFilesystemDefinition->addMethodCall(
					'prependPath',
					[ $easyAdminExtensionTwigPath, 'EasyAdmin' ]
				);
			}

			$nativeEasyAdminBundleRefl = new \ReflectionClass( EasyAdminBundle::class );
			if ( $nativeEasyAdminBundleRefl->isUserDefined() ) {
				$nativeEasyAdminBundlePath = \dirname( (string) $nativeEasyAdminBundleRefl->getFileName() );
				$nativeEasyAdminTwigPath   = $nativeEasyAdminBundlePath . '/Resources/views';
				// Defines a namespace from native EasyAdmin templates
				$twigLoaderFilesystemDefinition->addMethodCall(
					'addPath',
					[ $nativeEasyAdminTwigPath, 'BaseEasyAdmin' ]
				);
			}
		}
	}
