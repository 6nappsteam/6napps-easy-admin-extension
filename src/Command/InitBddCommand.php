<?php

	namespace Sixnapps\EasyadminExtensionBundle\Command;

	use Doctrine\ORM\EntityManagerInterface;
	use Symfony\Component\Console\Command\Command;
	use Symfony\Component\Console\Input\ArrayInput;
	use Symfony\Component\Console\Input\InputInterface;
	use Symfony\Component\Console\Output\OutputInterface;

	/**
	 * Class InitBddCommand
	 *
	 * @package Sixnapps\EasyadminExtensionBundle\Command
	 */
	class InitBddCommand extends Command
	{
		// the name of the command (the part after "bin/console")
		/**
		 * @var string
		 */
		protected static $defaultName = 'sixnapps:init-mysql';

		/**
		 * @var EntityManagerInterface
		 */
		private $entityManager;


		/**
		 * FormationController constructor.
		 *
		 * @param EntityManagerInterface $entityManager
		 */
		public function __construct(EntityManagerInterface $entityManager)
		{
			parent::__construct();

			$this->entityManager = $entityManager;
		}


		/**
		 *
		 */
		protected function configure()
		{
			$this
				// the short description shown while running "php bin/console list"
				->setDescription( 'Initialisation de la base de données.' )
				// the full command description shown when running the command with
				// the "--help" option
				->setHelp( 'Cette commande permet de réinitialiser la bdd' )
			;
		}


		/**
		 * @param InputInterface  $input
		 * @param OutputInterface $output
		 *
		 * @return int|void|null
		 * @throws \Exception
		 */
		protected function execute(InputInterface $input, OutputInterface $output)
		{
			/**
			 *
			 */
			$output->writeln( [ 'Supprime la base de données' ] );

			$command    = $this->getApplication()->find( 'doctrine:database:drop' );
			$arguments  = [
				'command'     => 'doctrine:database:drop',
				'--force'     => TRUE,
				'--if-exists' => TRUE,
			];
			$greetInput = new ArrayInput( $arguments );
			$command->run( $greetInput, $output );

			/**
			 *
			 */
			$output->writeln( [ 'Crée la base de données vide' ] );

			$command    = $this->getApplication()->find( 'doctrine:database:create' );
			$arguments  = [
				'command' => 'doctrine:database:create',
			];
			$greetInput = new ArrayInput( $arguments );
			$command->run( $greetInput, $output );

			/**
			 *
			 */
			$output->writeln( [ 'Importe le fichier sql' ] );
			$command   = $this->getApplication()->find( 'doctrine:database:import' );
			$arguments = [
				'command' => 'doctrine:database:import',
				'file'    => 'config/sql/dump.sql',
			];

			$greetInput = new ArrayInput( $arguments );
			$command->run( $greetInput, $output );
		}
	}
