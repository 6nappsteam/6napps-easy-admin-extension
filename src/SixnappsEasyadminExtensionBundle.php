<?php

	namespace Sixnapps\EasyadminExtensionBundle;

	use Sixnapps\EasyadminExtensionBundle\DependencyInjection\Compiler\TwigPathPass;
	use Symfony\Component\DependencyInjection\Compiler\PassConfig;
	use Symfony\Component\DependencyInjection\ContainerBuilder;
	use Symfony\Component\HttpKernel\Bundle\Bundle;

	class SixnappsEasyadminExtensionBundle extends Bundle
	{
		public function build(ContainerBuilder $container)
		{
			parent::build($container);

			$container->addCompilerPass(new TwigPathPass(), PassConfig::TYPE_BEFORE_OPTIMIZATION);

		}
	}
